> This project is [moved to GitHub](https://github.com/gitthermal/thermal).

<p align="center">
	<a href="https://thermal.netlify.com">
		<img src="https://thermal.netlify.com/images/logo.png" alt="Thermal" height="72" />
	</a>
</p>
<h1 align="center">
	Thermal
</h1>


<p align="center">
	Thermal is an free and open source Electron-based app. It is written in JavaScript and uses Vue.js.
</p>

<p align="center">
  <a href="https://www.patreon.com/join/mittalyashu">
    <img src="https://img.shields.io/badge/become%20a-patron-blue.svg?style=flat&colorA=555555&colorB=F86754" alt="Become a Patron" />
  </a>
  <a href="https://gitlab.com/gitthermal/thermal/pipelines">
    <img src="https://gitlab.com/gitthermal/thermal/badges/master/pipeline.svg" alt="GitLab pipeline" />
  </a>
  <a href="https://discord.gg/DcSNmts">
    <img src="https://img.shields.io/discord/556376419886825509.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2" alt="Discord invite" />
  </a>
  <a href="https://www.notion.so/gitthermal/Contribute-9d82f521342f4573b853d1bc793bdf02">
    <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg" alt="PRs welcome!" />
  </a>
  <a href="https://twitter.com/intent/follow?screen_name=gitthermal">
    <img src="https://img.shields.io/twitter/follow/gitthermal.svg?label=Follow%20@gitthermal" alt="Follow @gitthermal" />
  </a>
</p>

<h3 align="center">
  <a href="https://thermal.netlify.com/win/guide/">Guide</a>
  <span> · </span>
  <a href="https://thermal.netlify.com/download/">Download</a>
  <span> · </span>
  <a href="https://thermal.netlify.com/issue/">Issues</a>
  <span> · </span>
  <a href="https://www.notion.so/gitthermal/Contribute-9d82f521342f4573b853d1bc793bdf02">Contribute</a>
  <span> · </span>
  <a href="https://discord.gg/DcSNmts">Discord</a>
</h3>
