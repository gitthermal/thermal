# Contributing

The documentation for contributing to the Thermal project has been moved to a [new location](https://www.notion.so/gitthermal/Contribute-9d82f521342f4573b853d1bc793bdf02).
